#include<stdio.h>
#include<stdlib.h>
#include<math.h>

float f(float x){
float pi=acos(-1.0);
return 5 * pow( sin(x/pi), 2);
}

int main() {

int i=0,z[4]={10,50,100,1000},j;
float a=0,b=1,n=10,x=0,width,S,F;

for (b=1;b<=5;b++){
for (j=0;j<4;j++){
n=z[j];
width = (b - a) / n;
S=f(a);


for (i=1;i<=n;i=i+2) {
	x = a + width * i;
	S = S + 4 * f(x);
}

for (i=2;i<n;i=i+2) {
	x = a + width * i;
	S = S + 2 * f(x);
}

S = S + f(b);
F = width * S / 3;

printf("voltage: %f after %f seconds with %f mesh points\n",F/10, b, n); //the /10 is for the capacitance in 1b
}
}

return 0;

}
